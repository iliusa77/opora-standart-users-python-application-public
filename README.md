This repository contains dockerized demo Python application

### Gitlab CI/CD variables
First need to add manually the following:

- SSH_PRIVATE_KEY (for connection to EC2 instance by SSH)
- EC2_SERVER_IP (for connection to EC2 instance by SSH)
- REPONAME (Dockerhub username)
- REPOTOKEN (Dockerhub password)

### Manual application run (in Vagrant vm)
Create Vagrant vm
```
vagrant up && vagrant ssh
```

Go to `users` microservice location
```
sudo -i
cd /vagrant/
```


Install Python modules
```
pip install -r requirements.txt
```

Run initial migrations
```
flask db init
flask db migrate -m "initial migration"
flask db upgrade
```

Run application
```
python3 app.py
```

### Manual application dockerization
build app image 
```
docker build -t iliusa77/python-users-app .
```

### Docker compose
Create external network
```
docker network create develop --subnet=172.20.0.0/16
```

Run compose stack
```
docker-compose up -d
```

Check stack status
```
docker-compose ps
         Name                       Command               State                     Ports                   
------------------------------------------------------------------------------------------------------------
vagrant_postgresql_1     docker-entrypoint.sh postgres    Up      0.0.0.0:5432->5432/tcp,:::5432->5432/tcp  
vagrant_python-users_1   /bin/sh ./entrypoint.sh          Up      0.0.0.0:32770->8080/tcp,:::32770->8080/tcp
vagrant_webserver_1      /docker-entrypoint.sh ngin ...   Up      0.0.0.0:80->80/tcp,:::80->80/tcp 
```

Open `users` microservice page
```
curl localhost
<!DOCTYPE html>
<html>
  <head>
    <title>Users</title>
  </head>
  <body>
    <h1>Users</h1>
    <table>
      <thead>
        <tr>
          <th>ID</th>
          <th>Username</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody>
        
      </tbody>
    </table>
    <br />
    <form action="/add_user" method="post">
      <button type="submit">Add User</button>
    </form>
    <br />
    <form action="/delete_users" method="post">
      <button type="submit">Delete Table</button>
    </form>
  </body>
```
