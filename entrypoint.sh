#!/bin/sh

flask db init
flask db migrate -m "initial migration"
flask db upgrade

python3 app.py