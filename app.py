from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__)
metrics = PrometheusMetrics(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://demouser:demopass@postgresql:5432/users'
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# static information as metric
metrics.info('app_info', 'Python Flask users microservice', version='1.0.0')


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255))
    email = db.Column(db.String(255))

@app.route('/')
@metrics.gauge('index_page', 'Open users microservice start page')
def index():
    users = User.query.all()
    return render_template('users.html', users=users)

@app.route('/add_user', methods=['GET', 'POST'])
@metrics.gauge('add_user', 'Add new user in users')
def add_user():
    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        age = request.form['age']
        user = User(username=username, email=email, age=age)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('add_user.html')


@app.route('/delete_users', methods=['POST'])
@metrics.gauge('delete_users', 'Delete all users')
def delete_users():
    db.drop_all()
    db.create_all()
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=8080)